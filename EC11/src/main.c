#include "main.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "stm32f1xx_hal.h"
#include "stm32f1xx_hal_tim.h"
#include "SysClk16MHz.h"
#include "MAX7219.h"
#include "DWT.h"

#ifdef DEBUG
extern void initialise_monitor_handles(void);
#endif

MAX7219_Def _MAX7219_1;

void Error_Handler(void);

void Error_Handler(void) {
	int i = 0;
	while (1) {
		i = 1;
	}
}

#ifdef USE_FULL_ASSERT

void assert_failed(uint8_t* file, uint32_t line)
{
}
#endif

int main(void) {
#ifdef DEBUG
	initialise_monitor_handles();
#endif

	HAL_Init();
	DWT_Init();
	SystemClock_HSE_PLLx2_16MHz();

	MAX7219_SPI1_Init(&_MAX7219_1);
	_MAX7219_1.count = 2;

	MAX7219_Reset(&_MAX7219_1);

	MAX7219_Value v0,v1;
	v0.chip = 0; v0.dotDigit = -1; v0.leadZero = true; v0.value = 1;
	v1.chip = 1; v1.dotDigit = -1; v1.leadZero = true; v1.value = 99999999;
	while (1) {
#ifdef DEBUG
		printf("hello world!\n");
#endif
		DWT_Delay(100000);
		MAX7219_SetIntValue(&_MAX7219_1,&v0);
		MAX7219_SetIntValue(&_MAX7219_1,&v1);
		v0.value++;
		v0.dotDigit = v0.value%8 + 1;
		v1.value--;
		v1.dotDigit = v1.value%8 + 1;
	}
}

void HAL_SPI_TxCpltCallback(SPI_HandleTypeDef *hspi2) {
	if (hspi2->Instance == _MAX7219_1.SPI) {
		MAX7219_TxCallback(&_MAX7219_1);
	}
}

