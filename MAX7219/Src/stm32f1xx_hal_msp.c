#include "stm32f1xx_hal.h"
#include "MAX7219.h"

extern MAX7219_Def _MAX7219_1;

void HAL_MspInit(void) {
	__HAL_RCC_AFIO_CLK_ENABLE()
	;

	HAL_NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);
	HAL_NVIC_SetPriority(MemoryManagement_IRQn, 0, 0);
	HAL_NVIC_SetPriority(BusFault_IRQn, 0, 0);
	HAL_NVIC_SetPriority(UsageFault_IRQn, 0, 0);
	HAL_NVIC_SetPriority(SVCall_IRQn, 0, 0);
	HAL_NVIC_SetPriority(DebugMonitor_IRQn, 0, 0);
	HAL_NVIC_SetPriority(PendSV_IRQn, 0, 0);
	HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);

#ifndef DEBUG
	__HAL_AFIO_REMAP_SWJ_DISABLE();
#endif
}

void HAL_SPI_MspInit(SPI_HandleTypeDef* spiHandle) {
	if (spiHandle->Instance == _MAX7219_1.SPI) {
		MAX7219_MspInit(&_MAX7219_1);
	}
}

void HAL_SPI_MspDeInit(SPI_HandleTypeDef* spiHandle) {
	if (spiHandle->Instance == _MAX7219_1.SPI) {
		MAX7219_MspDeInit(&_MAX7219_1);
	}
}
