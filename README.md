Usefull links
=============
* MAX7219
	* https://github.com/SERGIOKA83/STM32_MAX7219
	* https://github.com/petoknm/MAX7219
	* https://github.com/daxrt/wifi_clock_stm32
* TM1638 implemntations 
  * https://github.com/conejoninja/TM1638Plus
  * http://arduino-kit.ru/catalog/id/modul-klaviaturyi-i-svetodiodnoy-indikatsii-tm1638-led_key

* PWM (pulse-width modulation)
  * http://visualgdb.com/tutorials/arm/stm32/pwm/

* TIM 
  * http://electronics.stackexchange.com/questions/227596/direct-frequency-measurement-with-two-hw-timers 	 
  * [internal RC oscillator (HSI) calibration](http://www.st.com/content/ccc/resource/technical/document/application_note/d1/e4/00/57/3b/75/4d/0d/CD00221839.pdf/files/CD00221839.pdf/jcr:content/translations/en.CD00221839.pdf)  
  * [SysTick accuracy in STM32](http://electronics.stackexchange.com/questions/55468/systick-accuracy-in-stm32?rq=1)
  * [frequency calculation](http://electronics.stackexchange.com/questions/132000/stm32-series-microcontroller-calculations-of-timer-variables): 
  	TIMUpdateFrequency(Hz)=Clock/(Prescaler−1)∗(Period−1) 
  	Prescaler = ((((ClockSpeed) / ((period) / (1 / frequency))) + 0.5) - 1);
  	
  	1=16000000/(10001-1)*(1601-1) 
  * [Libraries for STM32F4xx and STM32F7xx built on HAL drivers from ST](https://github.com/MaJerle/stm32fxxx_hal_libraries)
  * [Controlling STM32 Hardware Timers using HAL](https://github.com/MaJerle/stm32fxxx_hal_libraries)
  * [General-purpose timer cookbook](http://www.st.com/content/ccc/resource/technical/document/application_note/group0/91/01/84/3f/7c/67/41/3f/DM00236305/files/DM00236305.pdf/jcr:content/translations/en.DM00236305.pdf)
  
Eclipse ARM
=========== 
* [gnuarmeclipse](http://gnuarmeclipse.github.io/)
* [openocd](https://github.com/gnuarmeclipse/openocd/releases)  
* [gcc-arm-embedded](https://launchpad.net/gcc-arm-embedded)
* [Semihosting on ARM with GCC and OpenOCD](http://bgamari.github.io/posts/2014-10-31-semihosting.html)
	* LDFLAGS += --specs=rdimon.specs -lc -lrdimon
	* Note that librdimon takes the place of libnosys.
	* extern void initialise_monitor_handles(void);
	* initialise_monitor_handles();
	
Tools
=====

* [Logic Sniffer Java client](https://www.lxtreme.nl/ols/)
* [Using STM32F4 as a logic analyser (SUMP/8channels/21Mhz) – (not 24Mhz)](http://jjmz.free.fr/?p=148)
* [STM32F4-based Logic Analyser](https://www.fussylogic.co.uk/blog/?p=1226)	
* [A SUMP compatible Logical Analyser for the STM32F4](https://github.com/jpbarraca/LogicAlNucleo)
* [Logic analyzer firmware for STM32F4Discovery board](https://code.google.com/archive/p/logicdiscovery/)
* [LogicDiscovery — простой логический анализатор](https://geektimes.ru/post/257728/)
	
Math
====
* [The "Average" of a Set—Different Ways to Measure Central Tendency](https://owlcation.com/stem/Different-Ways-to-Find-an-Average-Mean-Median-Mode-and-Midrange#mod_20733273)
* [How to improve ADC accuracy when using STM32F2xx and STM32F4xx microcontrollers](http://www.st.com/content/ccc/resource/technical/document/application_note/a0/71/3e/e4/8f/b6/40/e6/DM00050879.pdf/files/DM00050879.pdf/jcr:content/translations/en.DM00050879.pdf) - has c-example and some thiory
* [Fastest moving averaging techniques with minimum footprint](http://electronics.stackexchange.com/questions/42734/fastest-moving-averaging-techniques-with-minimum-footprint) - has examples and explanation of them