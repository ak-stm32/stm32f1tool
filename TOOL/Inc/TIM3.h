#ifndef TIM3_H_
#define TIM3_H_
#include "stm32f1xx_hal.h"

extern void Error_Handler(void);

void MX_TIM3_Init(void);
void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);
void MX_TIM3_Start(void);
void MX_TIM3_SetPeriod(uint32_t period);
void MX_TIM3_SetPrescaler(uint32_t prescaler);
void MX_TIM3_SetChannelPulse(uint32_t channel, uint32_t pulse);

//typedef struct TIM_Clock_Def {
//	uint32_t Period;
//	uint32_t Prescale;
//} TIM_Clock_Def;
//
//
//typedef struct TIM_Pulse_Def {
//	uint32_t Pulse;
//	uint32_t Channel;
//} TIM_Pulse_Def;

#endif
