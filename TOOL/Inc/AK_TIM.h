/*
 * AK_TIM.h
 *
 *  Created on: 30 Jul 2016
 *      Author: akoiro
 */

#ifndef AK_TIM_H_
#define AK_TIM_H_
#include "COMMON.h"

typedef struct AK_TIM_Def{
	TIM_TypeDef *Instance;
	uint32_t Prescaler;
	uint32_t Period;
	IRQn_Type IRQn;

	TIM_HandleTypeDef Handle;
} AK_TIM_Def;

void AK_TIM_Init(AK_TIM_Def*);
void AK_TIM_IRQHandler(AK_TIM_Def*);
void AK_TIM_MspInit(AK_TIM_Def*);
void AK_TIM_Start_IT(AK_TIM_Def *def);
void AK_TIM_SetPeriod(AK_TIM_Def* def, uint32_t period);
void AK_TIM_SetPrescaler(AK_TIM_Def* def, uint32_t prescaler);

#endif /* AK_TIM_H_ */
