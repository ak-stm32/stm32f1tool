/*
 * TM1638.h
 *
 *  Created on: 17 ���. 2016 �.
 *      Author: pervoliner
 */

#ifndef TM1638_H_
#define TM1638_H_

#include <stdbool.h>
#include "stm32f1xx_hal.h"
#include "COMMON.h"

typedef struct {
	GPIOPin_Def PinSTB;

	GPIOPin_Def PinCLK;

	GPIOPin_Def PinDIO;
} TM1638_Def;

typedef enum {
	TM1638_B00 = 0,
	TM1638_B01 = 1,
	TM1638_B02 = 2,
	TM1638_B03 = 3,
	TM1638_B04 = 4,
	TM1638_B05 = 5,
	TM1638_B06 = 6,
	TM1638_B07 = 7
} TM1638_Button;

typedef enum {
	PRESSED = 1, RELEASED = 0
} TM1638_ButtonState;

void TM1638_Init(TM1638_Def*);
void TM1638_SetBrightness(TM1638_Def* def, uint8_t brightnes);
void TM1638_SetValue(TM1638_Def* def, uint32_t value, bool leadZero);
void TM1638_SetLED(TM1638_Def* def, uint8_t led, bool set);
TM1638_ButtonState TM1638_GetButtonStatus(TM1638_Def* def, TM1638_Button b);

#endif
