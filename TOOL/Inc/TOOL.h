/*
 * TOOL.h
 *
 *  Created on: 25 ���. 2016 �.
 *      Author: pervoliner
 */

#ifndef TOOL_H_
#define TOOL_H_

#include "AK_TIM.h"
#include "TM1638.h"

#define MAIN_TIMER  		TIM2
#define MAIN_TIMER_IRQ  	TIM2_IRQn
#define MAIN_TIMER_PERIOD  	15900
#define MAIN_TIMER_PRESCALE 1001


//Display
#define DISPLAY_STB_PORT	GPIOB
#define DISPLAY_STB_PIN		GPIO_PIN_12

#define DISPLAY_CLK_PORT	GPIOB
#define DISPLAY_CLK_PIN		GPIO_PIN_13

#define DISPLAY_DIO_PORT	GPIOB
#define DISPLAY_DIO_PIN		GPIO_PIN_14

#define DISPLAY_BRIGHTNESS	7

//Sensor
#define SENSOR_PORT		GPIOA
#define SENSOR_PIN		GPIO_PIN_1
#define SENSOR_IRQ 		EXTI1_IRQn




#define TIM_FREQ  1000000

#define AVERAGING_SAMPLES 4

void TOOL_Init(void);
void TOOL_Start(void);
void TOOL_SYSTICK_Callback(void);
void TOOL_MainTimer_Callback(void);

typedef struct {
	TM1638_Button button;
	TM1638_ButtonState state;
	bool changed;
	uint32_t count;
} Button_Handle;


typedef struct {
	uint32_t SensorTickCount;
	uint32_t MainTimerTickCount;

	uint32_t value;
	uint32_t speedValue;

	Button_Handle B0;
	Button_Handle B1;
	Button_Handle B2;
	Button_Handle B3;
	Button_Handle B4;
	Button_Handle B5;
	Button_Handle B6;
	Button_Handle B7;
	Button_Handle B8;

	TM1638_Def display;

	AK_TIM_Def timer;

	//sensor port
	uint32_t SensorPin;
	GPIO_TypeDef SensorPort;
	IRQn_Type SensorIRQ;
} TOOL_Def;

#endif /* TOOL_H_ */
