/*
 * TM1638.c
 *
 *  Created on: 17 ���. 2016 �.
 *      Author: pervoliner
 */

//#ifdef TOOL
#include "TM1638.h"
#include "TM1638Font.h"
#include "DWT.h"

#define MS_TIMES HAL_RCC_GetHCLKFreq() / 1000000
#define TICK_US 1

void __GPIO_PORT_ENABLE(GPIO_TypeDef*);
void __TM1638_InitPin(GPIOPin_Def* pin);
void __TM1638_Delay(uint32_t);

//write functions
void __TM1638_WriteLED(TM1638_Def* def, uint8_t digit, bool value);
void __TM1638_WriteDigit(TM1638_Def* def, uint8_t digit, uint8_t value);
void __TM1638_WriteValue(TM1638_Def* def, uint8_t addr, uint8_t value);
void __TM1638_WriteCommand(TM1638_Def*, uint8_t);
void __TM1638_Write(TM1638_Def*, uint8_t);
void __TM1638_WritePin(GPIOPin_Def*, GPIO_PinState);

//read functions
GPIO_PinState __TM1638_ReadPin(GPIOPin_Def*);
uint8_t __TM1638_Read(TM1638_Def*);
uint8_t __TM1638_ReadValue(TM1638_Def*);


void TM1638_SetBrightness(TM1638_Def* def, uint8_t brightnes) {
	__TM1638_WriteCommand(def, 0x40);
	__TM1638_WriteCommand(def, 0x80 | 8 | brightnes );
}

void TM1638_SetValue(TM1638_Def* def, uint32_t value, bool leadZero) {
	__TM1638_WriteCommand(def, 0x40);

	uint8_t d = 8;
	while (d > 0) {
		uint8_t dv = value%10;

		uint8_t dc =  NUMBER_FONT[dv];

		if (value == 0 && d != 8 && !leadZero) dc = 0;

		__TM1638_WriteDigit(def, d-1, dc);

		value /= 10;

		d--;
	}
}

void TM1638_SetLED(TM1638_Def* def, uint8_t led, bool set) {
	__TM1638_WriteLED(def, led, set);
}

TM1638_ButtonState TM1638_GetButtonStatus(TM1638_Def* def, TM1638_Button b) {
	uint8_t value = __TM1638_ReadValue(def);
	return (value & ( 1 << b )) >> b;
}


void TM1638_Init(TM1638_Def* def) {

	__TM1638_InitPin(&def->PinSTB);
	__TM1638_InitPin(&def->PinCLK);
	__TM1638_InitPin(&def->PinDIO);

	__TM1638_WritePin(&def->PinSTB, GPIO_PIN_SET);
	__TM1638_WritePin(&def->PinCLK, GPIO_PIN_SET);
	__TM1638_Delay(TICK_US);

	__TM1638_WriteDigit(def, 0, NUMBER_FONT[0]);
	__TM1638_WriteDigit(def, 1, NUMBER_FONT[0]);
	__TM1638_WriteDigit(def, 2, NUMBER_FONT[0]);
	__TM1638_WriteDigit(def, 3, NUMBER_FONT[0]);
	__TM1638_WriteDigit(def, 4, NUMBER_FONT[0]);
	__TM1638_WriteDigit(def, 5, NUMBER_FONT[0]);
	__TM1638_WriteDigit(def, 6, NUMBER_FONT[0]);
	__TM1638_WriteDigit(def, 7, NUMBER_FONT[0]);

	__TM1638_WriteLED(def, 0, false);
	__TM1638_WriteLED(def, 1, true);
	__TM1638_WriteLED(def, 2, false);
	__TM1638_WriteLED(def, 3, true);
	__TM1638_WriteLED(def, 4, false);
	__TM1638_WriteLED(def, 5, true);
	__TM1638_WriteLED(def, 6, false);
	__TM1638_WriteLED(def, 7, true);
}

void __TM1638_InitPin(GPIOPin_Def* pin) {

	GPIO_InitTypeDef gpioDef;

	gpioDef.Pull = GPIO_PULLUP;
	gpioDef.Mode = GPIO_MODE_OUTPUT_OD; // OD = open drain
	gpioDef.Speed = GPIO_SPEED_FREQ_HIGH;
	gpioDef.Pin = pin->pin;
    HAL_GPIO_Init(pin->port, &gpioDef);
}

void __TM1638_WriteDigit(TM1638_Def* def, uint8_t digit, uint8_t value) {
	__TM1638_WriteValue(def, digit << 1, value);
}

void __TM1638_WriteLED(TM1638_Def* def, uint8_t digit, bool value) {
	__TM1638_WriteValue(def, (digit << 1) + 1, value);
}

/**
 * Write value to special digit
 */
void __TM1638_WriteValue(TM1638_Def* def, uint8_t addr, uint8_t value) {
	__TM1638_WriteCommand(def, 0x44);

	__TM1638_WritePin(&def->PinSTB, GPIO_PIN_RESET);

	__TM1638_Write(def, 0xC0 | addr);

	__TM1638_Write(def, value);

	__TM1638_WritePin(&def->PinSTB, GPIO_PIN_SET);

	__TM1638_Delay(TICK_US);
}

void __TM1638_WriteCommand(TM1638_Def* def, uint8_t command) {
	__TM1638_WritePin(&def->PinSTB, GPIO_PIN_RESET);
	__TM1638_Delay(TICK_US);
	__TM1638_Write(def, command);
	__TM1638_WritePin(&def->PinSTB, GPIO_PIN_SET);
	__TM1638_Delay(TICK_US);
}

void __TM1638_Write(TM1638_Def* def, uint8_t data) {
	 for (int i = 0; i < 8; i++) {
		 __TM1638_WritePin(&def->PinCLK, GPIO_PIN_RESET);
		 __TM1638_WritePin(&def->PinDIO, data & 1 ? GPIO_PIN_SET : GPIO_PIN_RESET);
		 __TM1638_Delay(TICK_US);
		 data >>= 1;
		 __TM1638_WritePin(&def->PinCLK, GPIO_PIN_SET);
		__TM1638_Delay(TICK_US);
	 }
}

void __TM1638_WritePin(GPIOPin_Def* pin, GPIO_PinState state) {
	HAL_GPIO_WritePin(pin->port, pin->pin, state);
}

GPIO_PinState __TM1638_ReadPin(GPIOPin_Def* pin) {
	return	HAL_GPIO_ReadPin(pin->port, pin->pin);
}

/*
 * +byte TM1638::getButtons(void)
 +{
 +  byte keys = 0;
 +
 +  digitalWrite(strobePin, LOW);
 +  send(0x42);
 +  for (int i = 0; i < 4; i++) {
 +    keys |= receive() << i;
 +  }
 +  digitalWrite(strobePin, HIGH);
 +
 +  return keys;
 +}
 */

uint8_t __TM1638_ReadValue(TM1638_Def* def) {
	uint8_t result = 0x00;

	__TM1638_WritePin(&def->PinSTB, GPIO_PIN_RESET);
	__TM1638_Delay(TICK_US);

	__TM1638_Write(def, 0x42);
	for (int i = 0; i< 4; i++) {
		result |= __TM1638_Read(def) << i;
	}
	__TM1638_WritePin(&def->PinSTB, GPIO_PIN_SET);
	__TM1638_Delay(TICK_US);
	return result;
}

uint8_t __TM1638_Read(TM1638_Def* def) {
	uint8_t result = 0x00;
	__TM1638_WritePin(&def->PinDIO, GPIO_PIN_SET);
	__TM1638_Delay(TICK_US);
	for (uint8_t i=0; i<8;i++) {
		result >>= 1;
		__TM1638_WritePin(&def->PinCLK, GPIO_PIN_RESET);
		__TM1638_Delay(TICK_US);

		if (__TM1638_ReadPin(&def->PinDIO) == GPIO_PIN_SET) {
			result |= 0x80;
		}
		__TM1638_Delay(TICK_US);

		__TM1638_WritePin(&def->PinCLK, GPIO_PIN_SET);
		__TM1638_Delay(TICK_US);
	}
	__TM1638_WritePin(&def->PinDIO, GPIO_PIN_RESET);
	__TM1638_Delay(TICK_US);
	return result;
}


void __TM1638_Delay(uint32_t us) {
	DWT_Delay(us);
}
//#endif
