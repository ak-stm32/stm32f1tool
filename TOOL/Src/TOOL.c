/*
 * TOOL.c
 *
 *  Created on: 25 ���. 2016 �.
 *      Author: pervoliner
 */

#include "TOOL.h"
#include "DWT.h"


TOOL_Def TOOL;

void _TOOL_InitDisplay(void);
void _TOOL_InitTimer(void);
void _TOOL_ResetButton(Button_Handle* handle);
void _TOOL_HandleKey(Button_Handle* handle, void (*function)(uint32_t));
void _TOOL_RefreshDisplay(void);
void _TOOL_HandleKeys(void);
void _TOOL_InitSensor(void);
uint32_t _TOOL_Averaging(uint32_t average, uint32_t new, uint8_t count);

void TOOL_Init(void) {
	DWT_Init();

	TOOL.B0.button = TM1638_B00;
	TOOL.B1.button = TM1638_B01;
	TOOL.B2.button = TM1638_B02;
	TOOL.B3.button = TM1638_B03;
	TOOL.B4.button = TM1638_B04;
	TOOL.B5.button = TM1638_B05;
	TOOL.B6.button = TM1638_B06;
	TOOL.B7.button = TM1638_B07;

	_TOOL_ResetButton(&TOOL.B0);
	_TOOL_ResetButton(&TOOL.B1);
	_TOOL_ResetButton(&TOOL.B2);
	_TOOL_ResetButton(&TOOL.B3);
	_TOOL_ResetButton(&TOOL.B4);
	_TOOL_ResetButton(&TOOL.B5);
	_TOOL_ResetButton(&TOOL.B6);
	_TOOL_ResetButton(&TOOL.B7);

	_TOOL_InitDisplay();

	_TOOL_InitTimer();

	_TOOL_InitSensor();
}

void TOOL_Start(void) {
	AK_TIM_Start_IT(&TOOL.timer);
}

void TOOL_SYSTICK_Callback(void) {
	TOOL.SensorTickCount++;
}

void TOOL_MainTimer_Callback(void) {

	TOOL.speedValue = _TOOL_Averaging(TOOL.speedValue, TOOL.SensorTickCount, AVERAGING_SAMPLES);
	//TOOL.SensorTickCount = 0;
	TOOL.MainTimerTickCount++;
	_TOOL_HandleKeys();
	_TOOL_RefreshDisplay();
}


void _TOOL_InitSensor(void) {

	GPIO_InitTypeDef str;

	str.Pin = SENSOR_PIN;
	str.Mode = GPIO_MODE_IT_FALLING;
	str.Speed = GPIO_SPEED_FREQ_HIGH;
	str.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(SENSOR_PORT, &str);

	HAL_NVIC_SetPriority(SENSOR_IRQ, 10, 10);
	HAL_NVIC_EnableIRQ(SENSOR_IRQ);

}

void _TOOL_InitTimer(void) {

	TOOL.timer.Instance = MAIN_TIMER;
	TOOL.timer.Period = MAIN_TIMER_PERIOD;
	TOOL.timer.Prescaler = MAIN_TIMER_PRESCALE;
	TOOL.timer.IRQn = MAIN_TIMER_IRQ;
	AK_TIM_Init(&TOOL.timer);
}

void _TOOL_ResetButton(Button_Handle* handle) {
	handle->changed = false;
	handle->count = 0;
	handle->state = RELEASED;
}

void _TOOL_InitDisplay(void) {
	TOOL.display.PinSTB.port = DISPLAY_STB_PORT;
	TOOL.display.PinSTB.pin = DISPLAY_STB_PIN;
	TOOL.display.PinCLK.port = DISPLAY_CLK_PORT;
	TOOL.display.PinCLK.pin = DISPLAY_CLK_PIN;
	TOOL.display.PinDIO.port = DISPLAY_DIO_PORT;
	TOOL.display.PinDIO.pin = DISPLAY_DIO_PIN;
	TM1638_Init(&TOOL.display);

	TM1638_SetBrightness(&TOOL.display, DISPLAY_BRIGHTNESS);
	TM1638_SetValue(&TOOL.display, 0, false);
}

void _TOOL_IncrementValue(uint32_t v) {
	if (TOOL.value >= 99999999)
		return;
	TOOL.value += v;
}

void _TOOL_DecrementValue(uint32_t v) {
	if (TOOL.value == 0)
		return;

	TOOL.value -= v;
}

void _TOOL_ResetValue(uint32_t v) {
	TOOL.value = 0;
	TOOL.MainTimerTickCount = 0;
	TOOL.SensorTickCount = 0;
}

void _TOOL_RefreshDisplay() {
	TM1638_ButtonState state = TM1638_GetButtonStatus(&TOOL.display,
			TOOL.B3.button);
	switch (state) {
	case PRESSED:
		TM1638_SetValue(&TOOL.display, TOOL.SensorTickCount, false);
		break;
	default:
		TM1638_SetValue(&TOOL.display, TOOL.MainTimerTickCount*10000 + TOOL.SensorTickCount, false);
		break;
	}
}

void _TOOL_HandleKeys(void) {

	_TOOL_HandleKey(&TOOL.B0, _TOOL_IncrementValue);

	_TOOL_HandleKey(&TOOL.B1, _TOOL_DecrementValue);

	_TOOL_HandleKey(&TOOL.B2, _TOOL_ResetValue);
}

void _TOOL_HandleKey(Button_Handle* handle, void (*function)(uint32_t)) {

	TM1638_ButtonState state = TM1638_GetButtonStatus(&TOOL.display,
			handle->button);

	if (state == handle->state) {
		handle->count++;
		handle->changed = false;
	} else {
		handle->state = state;
		handle->count = 0;
		handle->changed = true;
	}

	switch (handle->state) {
	case PRESSED:
		if (handle->changed) {
			function(1);
		} else if (handle->count > 100 && handle->count < 1000
				&& handle->count % 10 == 0) {
			function(1);
		} else if (handle->count > 1000) {
			function(1);
		}
		break;
	case RELEASED:
		_TOOL_ResetButton(handle);
		break;
	}
}

uint32_t _TOOL_Averaging(uint32_t average, uint32_t new, uint8_t count) {
	if (average == 0) {
		return new;
	} else {
		return (new >> count) + average - (average >>  count);
	}
}
